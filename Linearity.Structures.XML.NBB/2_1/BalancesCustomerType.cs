﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public class BalancesCustomerType
    {
        /// <remarks />
        public CustomerIdentificationType CustomerIdentification { get; set; }

        /// <remarks />
        [XmlElement("Contract")]
        public BalancesContractType[] Contract { get; set; }

        /// <remarks />
        [XmlAttribute(DataType = "int")]
        public int CustomerSequenceNumber { get; set; }

        /// <remarks />
        [XmlAttribute]
        public string CustomerBankIdentification { get; set; }
    }
}