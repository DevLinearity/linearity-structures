﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;
using Linearity.Structures.XML.NBB._2_1.Contracts;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public class CloseActionType : IActionType
    {
        /// <remarks />
        [XmlArrayItem("Account", IsNullable = false)]
        public CloseAccountType[] Accounts { get; set; }

        /// <remarks />
        [XmlArrayItem("Contract", IsNullable = false)]
        public CloseContractType[] Contracts { get; set; }
    }
}