﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    [XmlRoot("Header", Namespace = "http://www.nbb.be/cba/2019-04/declaration", IsNullable = false)]
    public class HeaderType
    {
        /// <remarks />
        [XmlElement("DeclarerId", typeof(DeclarerIdType))]
        public DeclarerIdType DeclarerId { get; set; }

        /// <remarks />
        [XmlElement(DataType = "date")]
        public DateTime ReportingDate { get; set; }

        /// <remarks />
        [XmlElement]
        public string FileNumber { get; set; }

        /// <remarks />
        [XmlElement]
        public string FileReference { get; set; }
    }
}