﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;
using Linearity.Structures.XML.NBB._2_1.Enum;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public abstract class CashTransactionCustomerRoleType
    {
        /// <remarks />
        [XmlElement("Customer", typeof(string))]
        [XmlElement("Representative", typeof(string))]
        [XmlChoiceIdentifier("CustomerRoleTypeElementName")]
        public string Item => CustomerRole.ToString();

        /// <remarks />
        [XmlIgnore]
        public abstract CustomerRoleChoice CustomerRole { get; set; }
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Global
    public class CustomerCashTransactionCustomerRoleType : CashTransactionCustomerRoleType
    {
        [XmlIgnore]
        public override CustomerRoleChoice CustomerRole { get; set; }
            = CustomerRoleChoice.Customer;
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Global
    public class RepresentativeCashTransactionCustomerRoleType : CashTransactionCustomerRoleType
    {
        [XmlIgnore]
        public override CustomerRoleChoice CustomerRole { get; set; }
            = CustomerRoleChoice.Representative;
    }
}