﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;
using Linearity.Structures.XML.NBB._2_1.Enum;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public abstract class BalancesContractTypeNameType
    {
        /// <remarks />
        [XmlElement("InvestmentContract", typeof(string))]
        [XmlElement("LifeInsurance", typeof(string))]
        [XmlChoiceIdentifier("BalancesContractTypeChoice")]
        public string Item => BalancesContractTypeElementName.ToString();

        /// <remarks />
        [XmlIgnore]
        public abstract BalancesContractTypeChoice BalancesContractTypeElementName { get; set; }
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Global
    public class InvestmentContractBalancesContractTypeNameType : BalancesContractTypeNameType
    {
        [XmlIgnore]
        public override BalancesContractTypeChoice BalancesContractTypeElementName { get; set; } 
            = BalancesContractTypeChoice.InvestmentContract;
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Global
    public class LifeInsuranceBalancesContractTypeNameType : BalancesContractTypeNameType
    {
        [XmlIgnore]
        public override BalancesContractTypeChoice BalancesContractTypeElementName { get; set; }
            = BalancesContractTypeChoice.LifeInsurance;
    }
}