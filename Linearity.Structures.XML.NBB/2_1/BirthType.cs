﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public class BirthType
    {
        /// <remarks />
        [XmlElement("BirthDate", typeof(DateTime), DataType = "date")]
        [XmlElement("BirthYear", typeof(string), DataType = "gYear")]
        public object Item
        {
            get
            {
                if (BirthDate != null)
                    return BirthDate;
                return BirthYear;
            }
        }

        [XmlIgnore]
        public DateTime? BirthDate { get; set; }

        [XmlIgnore]
        public int? BirthYear { get; set; }
    }
}