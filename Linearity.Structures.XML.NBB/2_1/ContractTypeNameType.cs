﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;
using Linearity.Structures.XML.NBB._2_1.Enum;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    [XmlInclude(typeof(InstalmentLoanContractTypeNameType))]
    [XmlInclude(typeof(InstalmentSaleContractTypeNameType))]
    [XmlInclude(typeof(InvestmentContractContractTypeNameType))]
    [XmlInclude(typeof(LeasingContractContractTypeNameType))]
    [XmlInclude(typeof(LifeInsuranceContractTypeNameType))]
    [XmlInclude(typeof(MortgageLoanContractTypeNameType))]
    [XmlInclude(typeof(OverdraftFacilityContractTypeNameType))]
    [XmlInclude(typeof(ProfessionalLoanContractTypeNameType))]
    [XmlInclude(typeof(SafeDepositBoxContractTypeNameType))]
    public class ContractTypeNameType
    {
        /// <remarks />
        [XmlElement("InstalmentLoan", typeof(string))]
        [XmlElement("InstalmentSale", typeof(string))]
        [XmlElement("InvestmentContract", typeof(string))]
        [XmlElement("LeasingContract", typeof(string))]
        [XmlElement("LifeInsurance", typeof(string))]
        [XmlElement("MortgageLoan", typeof(string))]
        [XmlElement("OverdraftFacility", typeof(string))]
        [XmlElement("ProfessionalLoan", typeof(string))]
        [XmlElement("SafeDepositBox", typeof(string))]
        [XmlChoiceIdentifier("ContractTypeNameChoice")]
        public string Item => ContractTypeNameElementName.ToString();

        /// <remarks />
        [XmlIgnore]
        public virtual ContractTypeNameChoice ContractTypeNameElementName { get; set; }
    }

    // ReSharper disable once UnusedMember.Global
    // ReSharper disable once IdentifierTypo
    public class InstalmentLoanContractTypeNameType : ContractTypeNameType
    {
        [XmlIgnore]
        public override ContractTypeNameChoice ContractTypeNameElementName { get; set; }
            = ContractTypeNameChoice.InstalmentLoan;
    }

    // ReSharper disable once UnusedMember.Global
    public class InstalmentSaleContractTypeNameType : ContractTypeNameType
    {
        [XmlIgnore]
        public override ContractTypeNameChoice ContractTypeNameElementName { get; set; }
            = ContractTypeNameChoice.InstalmentLoan;
    }

    // ReSharper disable once UnusedMember.Global
    public class InvestmentContractContractTypeNameType : ContractTypeNameType
    {
        [XmlIgnore]
        public override ContractTypeNameChoice ContractTypeNameElementName { get; set; }
            = ContractTypeNameChoice.InvestmentContract;
    }

    // ReSharper disable once UnusedMember.Global
    public class LeasingContractContractTypeNameType : ContractTypeNameType
    {
        [XmlIgnore]
        public override ContractTypeNameChoice ContractTypeNameElementName { get; set; }
            = ContractTypeNameChoice.LeasingContract;
    }

    // ReSharper disable once UnusedMember.Global
    public class LifeInsuranceContractTypeNameType : ContractTypeNameType
    {
        [XmlIgnore]
        public override ContractTypeNameChoice ContractTypeNameElementName { get; set; }
            = ContractTypeNameChoice.LifeInsurance;
    }

    // ReSharper disable once UnusedMember.Global
    public class MortgageLoanContractTypeNameType : ContractTypeNameType
    {
        [XmlIgnore]
        public override ContractTypeNameChoice ContractTypeNameElementName { get; set; }
            = ContractTypeNameChoice.MortgageLoan;
    }


    // ReSharper disable once UnusedMember.Global
    public class OverdraftFacilityContractTypeNameType : ContractTypeNameType
    {
        [XmlIgnore]
        public override ContractTypeNameChoice ContractTypeNameElementName { get; set; }
            = ContractTypeNameChoice.OverdraftFacility;
    }

    // ReSharper disable once UnusedMember.Global
    public class ProfessionalLoanContractTypeNameType : ContractTypeNameType
    {
        [XmlIgnore]
        public override ContractTypeNameChoice ContractTypeNameElementName { get; set; }
            = ContractTypeNameChoice.ProfessionalLoan;
    }

    // ReSharper disable once UnusedMember.Global
    public class SafeDepositBoxContractTypeNameType : ContractTypeNameType
    {
        [XmlIgnore]
        public override ContractTypeNameChoice ContractTypeNameElementName { get; set; }
            = ContractTypeNameChoice.SafeDepositBox;
    }
}