﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public class NaturalPersonDetailsType
    {
        /// <remarks />
        public string FirstName { get; set; }

        /// <remarks />
        public string LastName { get; set; }

        /// <remarks />
        public BirthType BirthDay { get; set; }

        /// <remarks />
        public BirthPlaceType BirthPlace { get; set; }
    }
}