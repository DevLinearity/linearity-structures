﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace Linearity.Structures.XML.NBB._2_1.Enum
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration", IncludeInSchema = false)]
    public enum BalancesContractTypeChoice
    {
        /// <remarks />
        InvestmentContract,

        /// <remarks />
        LifeInsurance
    }
}