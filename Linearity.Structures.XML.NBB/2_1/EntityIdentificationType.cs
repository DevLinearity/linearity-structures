﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public class EntityIdentificationType
    {
        /// <remarks />
        [XmlElement("EntityDetailsIdentification", typeof(EntityDetailsType))]
        [XmlElement("KBONumberIdentification", typeof(string))]
        public object Item
        {
            get
            {
                if (EntityDetailsIdentification != null)
                    return EntityDetailsIdentification;
                return KBONumberIdentification;
            }
        }

        [XmlIgnore]
        public EntityDetailsType EntityDetailsIdentification { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public string KBONumberIdentification { get; set; }
    }
}