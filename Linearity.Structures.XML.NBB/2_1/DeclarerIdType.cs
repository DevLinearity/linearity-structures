﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;
using Linearity.Structures.XML.NBB._2_1.Enum;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    [XmlInclude(typeof(DeclarerKBONumberDeclarerIdType))]
    [XmlInclude(typeof(DeclarerLEICodeDeclarerIdType))]
    public class DeclarerIdType
    {
        /// <remarks />
        [XmlElement("DeclarerKBONumber", typeof(string))]
        [XmlElement("DeclarerLEICode", typeof(string))]
        [XmlChoiceIdentifier("DeclarerTypeElementName")]
        public virtual string Item { get; }

        /// <remarks />
        [XmlIgnore]
        public virtual DeclarerTypeChoice DeclarerTypeElementName { get; }
    }

    /// <inheritdoc />
    // ReSharper disable once InconsistentNaming
    // ReSharper disable once UnusedMember.Global
    public class DeclarerKBONumberDeclarerIdType : DeclarerIdType
    {
        [XmlIgnore]
        public override DeclarerTypeChoice DeclarerTypeElementName
            => DeclarerTypeChoice.DeclarerKBONumber;

        [XmlIgnore]
        public string KBONumber { get; set; }

        [XmlElement("DeclarerKBONumber", typeof(string))]
        public override string Item => KBONumber;
    }

    /// <inheritdoc />
    // ReSharper disable once InconsistentNaming
    // ReSharper disable once UnusedMember.Global
    public class DeclarerLEICodeDeclarerIdType : DeclarerIdType
    {
        [XmlIgnore]
        public override DeclarerTypeChoice DeclarerTypeElementName
            => DeclarerTypeChoice.DeclarerLEICode;

        [XmlIgnore]
        public string LEICode { get; set; }

        [XmlElement("DeclarerLEICode", typeof(string))]
        public override string Item => LEICode;
    }
}