﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    [XmlRoot("CustomerIdentification", Namespace = "http://www.nbb.be/cba/2019-04/declaration",
        IsNullable = false)]
    public class CustomerIdentificationType
    {
        /// <remarks />
        [XmlElement("EntityIdentification", typeof(EntityIdentificationType))]
        [XmlElement("NaturalPersonId", typeof(NaturalPersonIdType))]
        public object Item
        {
            get
            {
                if (EntityIdentificationType != null)
                    return EntityIdentificationType;
                return NaturalPersonIdType;
            }
        }

        [XmlIgnore] public EntityIdentificationType EntityIdentificationType { get; set; }

        [XmlIgnore] public NaturalPersonIdType NaturalPersonIdType { get; set; }
    }
}