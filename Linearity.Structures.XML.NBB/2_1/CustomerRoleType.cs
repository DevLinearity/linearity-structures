﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;
using Linearity.Structures.XML.NBB._2_1.Enum;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public abstract class CustomerRoleType
    {
        /// <remarks />
        [XmlElement("Holder", typeof(string))]
        [XmlElement("Representative", typeof(string))]
        [XmlChoiceIdentifier("CustomerRoleTypeElementName")]
        public string Item => CustomerRoleTypeElementName.ToString();

        /// <remarks />
        [XmlIgnore]
        public abstract CustomerRoleTypeChoice CustomerRoleTypeElementName { get; set; }
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Global
    public class HolderCustomerRoleType : CustomerRoleType
    {
        [XmlIgnore]
        public override CustomerRoleTypeChoice CustomerRoleTypeElementName { get; set; }
            = CustomerRoleTypeChoice.Holder;
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Global
    public class RepresentativeCustomerRoleType : CustomerRoleType
    {
        [XmlIgnore]
        public override CustomerRoleTypeChoice CustomerRoleTypeElementName { get; set; }
            = CustomerRoleTypeChoice.Representative;
    }
}