﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;
using Linearity.Structures.XML.NBB._2_1.Enum;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public abstract class AccountType
    {
        /// <remarks />
        [XmlElement("IBANNumber", typeof(string))]
        [XmlElement("PaymentAccountID", typeof(string))]
        [XmlChoiceIdentifier("CustomerRoleTypeElementName")]
        public string Item => AccountTypeChoice.ToString();

        /// <remarks />
        [XmlIgnore]
        public abstract AccountTypeChoice AccountTypeChoice { get; set; }

        /// <remarks />
        public CustomerRoleType CustomerRole { get; set; }

        /// <remarks />
        [XmlElement(DataType = "date")]
        public DateTime CustomerStartDate { get; set; }

            /// <remarks />
        [XmlAttribute(DataType = "int")]
        public int RelationSequenceNumber { get; set; }
    }

    /// <inheritdoc />
    // ReSharper disable once InconsistentNaming
    // ReSharper disable once UnusedMember.Global
    public class IBANNumberAccountType : AccountType
    {
        [XmlIgnore]
        public override AccountTypeChoice AccountTypeChoice { get; set; }
            = AccountTypeChoice.IBANNumber;
    }

    /// <inheritdoc />
    // ReSharper disable once InconsistentNaming
    // ReSharper disable once UnusedMember.Global
    public class PaymentAccountIDAccountType : AccountType
    {
        [XmlIgnore]
        public override AccountTypeChoice AccountTypeChoice { get; set; }
            = AccountTypeChoice.PaymentAccountID;
    }
}