﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public class BalancesPeriodStatusType
    {
        /// <remarks />
        [XmlArrayItem("Account", IsNullable = false)]
        public AccountStatusType[] Accounts { get; set; }

        /// <remarks />
        [XmlArrayItem("Customer", IsNullable = false)]
        public CustomerRelationsType[] Contracts { get; set; }

        /// <remarks />
        [XmlAttribute(DataType = "date")]
        public DateTime ValueDate { get; set; }
    }
}