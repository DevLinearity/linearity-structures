﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;
using Linearity.Structures.XML.NBB._2_1.Enum;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public abstract class CashTransactionTypeNameType
    {
        /// <remarks />
        [XmlElement("AgainstPreciousMetal", typeof(string))]
        [XmlElement("OnAccount", typeof(string))]
        [XmlElement("OnlyCash", typeof(string))]
        [XmlElement("ViaTransfer", typeof(string))]
        [XmlChoiceIdentifier("CashTransactionTypeChoice")]
        public string Item => CashTransactionTypeChoice.ToString();

        /// <remarks />
        [XmlIgnore]
        public abstract CashTransactionTypeChoice CashTransactionTypeChoice { get; set; }
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Global
    public class AgainstPreciousMetalCashTransactionTypeNameType : CashTransactionTypeNameType
    {
        [XmlIgnore]
        public override CashTransactionTypeChoice CashTransactionTypeChoice { get; set; }
            = CashTransactionTypeChoice.AgainstPreciousMetal;
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Global
    public class OnAccountCashTransactionTypeNameType : CashTransactionTypeNameType
    {
        [XmlIgnore]
        public override CashTransactionTypeChoice CashTransactionTypeChoice { get; set; }
            = CashTransactionTypeChoice.OnAccount;
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Global
    public class OnlyCashCashTransactionTypeNameType : CashTransactionTypeNameType
    {
        [XmlIgnore]
        public override CashTransactionTypeChoice CashTransactionTypeChoice { get; set; }
            = CashTransactionTypeChoice.OnlyCash;
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Global
    public class ViaTransferCashTransactionTypeNameType : CashTransactionTypeNameType
    {
        [XmlIgnore]
        public override CashTransactionTypeChoice CashTransactionTypeChoice { get; set; }
            = CashTransactionTypeChoice.ViaTransfer;
    }
}