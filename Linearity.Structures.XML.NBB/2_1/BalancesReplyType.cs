﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    [XmlRoot("BalancesReply", Namespace = "http://www.nbb.be/cba/2019-04/declaration", IsNullable = false)]
    public class BalancesReplyType
    {
        /// <remarks />
        public HeaderType Header { get; set; }

        /// <remarks />
        public string Status { get; set; }

        /// <remarks />
        public string ReturnCode { get; set; }

        /// <remarks />
        public string ReturnDescription { get; set; }

        /// <remarks />
        public BalancesStatisticsType Statistics { get; set; }

        /// <remarks />
        [XmlArrayItem("Period", IsNullable = false)]
        public BalancesPeriodStatusType[] PeriodsStatus { get; set; }
    }
}