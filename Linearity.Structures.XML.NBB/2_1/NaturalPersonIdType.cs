﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;
using Linearity.Structures.XML.NBB._2_1.Contracts;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public class NaturalPersonIdType : IPersonType
    {
        /// <remarks />
        [XmlElement("NaturalPersonDetailsId", typeof(NaturalPersonDetailsType))]
        [XmlElement("RRNIdentification", typeof(string))]
        public object Item
        {
            get
            {
                if (NaturalPersonDetailsId != null)
                    return NaturalPersonDetailsId;
                return RRNIdentification;
            }
        }

        [XmlIgnore] public NaturalPersonDetailsType NaturalPersonDetailsId { get; set; }

        // ReSharper disable once InconsistentNaming
        [XmlIgnore] public string RRNIdentification { get; set; }
    }
}