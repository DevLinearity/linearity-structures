﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public class CashTransactionType
    {
        /// <remarks />
        public CashTransactionTypeNameType CashTransactionTypeName { get; set; }

        /// <remarks />
        public CashTransactionCustomerRoleType CustomerRole { get; set; }

        /// <remarks />
        [XmlElement(DataType = "date")]
        public DateTime TransactionStartDate { get; set; }

        /// <remarks />
        [XmlAttribute(DataType = "int")]
        public int RelationSequenceNumber { get; set; }
    }
}