﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;
using Linearity.Structures.XML.NBB._2_1.Contracts;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public class CustomerType
    {
        /// <remarks />
        public CustomerIdentificationType CustomerIdentification { get; set; }

        /// <remarks />
        [XmlArrayItem("AddAction", typeof(AddActionType), IsNullable = false)]
        [XmlArrayItem("CloseAction", typeof(CloseActionType), IsNullable = false)]
        public object[] CustomerActions
        {
            get => CustomerActionsHidden;
            set => CustomerActionsHidden = (IActionType[])value;
        }

        [XmlIgnore]
        public IActionType[] CustomerActionsHidden { get; set; }

        /// <remarks />
        [XmlAttribute(DataType = "integer")]
        public string CustomerSequenceNumber { get; set; }

        /// <remarks />
        [XmlAttribute]
        public string CustomerBankIdentification { get; set; }
    }
}