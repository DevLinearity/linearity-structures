﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Linearity.Structures.XML.NBB._2_1
{
    /// <remarks />
    [GeneratedCode("xsd", "4.8.3928.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.nbb.be/cba/2019-04/declaration")]
    public class StatisticsType
    {
        /// <remarks />
        [XmlElement(DataType = "int")]
        public int TotalRelationsNumber { get; set; }

        /// <remarks />
        [XmlElement(DataType = "int")]
        public int AcceptedRelationsNumber { get; set; }

        /// <remarks />
        [XmlElement(DataType = "int")]
        public int RejectedRelationsNumber { get; set; }
    }
}