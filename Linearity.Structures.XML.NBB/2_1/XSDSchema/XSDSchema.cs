﻿using System.IO;
using System.Xml;
using System.Xml.Schema;
using Linearity.Structures.XML.NBB.Properties;
using Linearity.Structures.XML.Validator;

namespace Linearity.Structures.XML.NBB._2_1.XSDSchema
{
    public class XSDSchema : IXSDSchema
    {
        public XmlSchemaSet SchemaSet
        {
            get
            {
                XmlSchemaSet xmlSchemaSet = new XmlSchemaSet();
                var schema = Resources.CAP2_1_DeclarationRequest_xsd;
                using(var memoryStream = new MemoryStream(schema))
                using(var reader = XmlReader.Create(memoryStream))
                {
                    xmlSchemaSet.Add("http://www.nbb.be/cba/2019-04/declaration", reader);
                }
                return xmlSchemaSet;
            }
        }
    }
}
