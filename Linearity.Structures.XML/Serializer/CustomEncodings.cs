﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linearity.Structures.XML.Serializer
{
    public static class CustomEncodings
    {
        public static Encoding UTF8GovernmentEncoding = new CustomUTF8GovernmentEncoding(false);
    }

    internal class CustomUTF8GovernmentEncoding : UTF8Encoding
    {
        public CustomUTF8GovernmentEncoding()
        {
        }

        public CustomUTF8GovernmentEncoding(bool encoderShouldEmitUTF8Identifier)
            : base(encoderShouldEmitUTF8Identifier)
        {
        }

        public override string WebName => base.WebName.ToUpperInvariant();
    }
}
