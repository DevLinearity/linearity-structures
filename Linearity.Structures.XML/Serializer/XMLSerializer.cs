﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Linearity.Structures.Serializer;

namespace Linearity.Structures.XML.Serializer
{
    // ReSharper disable once InconsistentNaming
    public interface IXMLSerializer<in TRootElement> : ICanSerialize<TRootElement>
        where TRootElement : IRootElement
    {
        byte[] Serialize(TRootElement rootElement, Encoding encoding);
        byte[] Serialize(TRootElement rootElement, XmlWriterSettings xmlWriterSettings);
    }

    public interface IXMLSerializer : IXMLSerializer<IRootElement>
    {
    }

    // ReSharper disable once InconsistentNaming
    public class XMLSerializer<TRootElement> : IXMLSerializer<TRootElement>
        where TRootElement : IRootElement
    {
        public byte[] Serialize(TRootElement rootElement, XmlWriterSettings xmlWriterSettings)
        {
            using (var stream = new MemoryStream())
            using (var writer = XmlWriter.Create(stream, xmlWriterSettings))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(TRootElement));
                serializer.Serialize(writer, rootElement);
                stream.Flush();
                return stream.ToArray();
            }
        }

        public byte[] Serialize(TRootElement rootElement, Encoding encoding)
            => Serialize(rootElement, new XmlWriterSettings { Encoding = encoding });

        public byte[] Serialize(TRootElement rootElement)
            => Serialize(rootElement, new UTF8Encoding(false));
    }

    public class XMLSerializer : XMLSerializer<IRootElement>, IXMLSerializer
    {
    }
}
