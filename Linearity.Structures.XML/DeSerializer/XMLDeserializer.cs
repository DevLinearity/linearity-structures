﻿using System.IO;
using System.Xml.Serialization;
using Linearity.Structures.DeSerializer;

namespace Linearity.Structures.XML.DeSerializer
{
    // ReSharper disable once InconsistentNaming
    public interface IXMLDeserializer<out T> : ICanDeSerialize<T>
        where T : IRootElement, new()
    {
    }

    // ReSharper disable once InconsistentNaming
    public class XMLDeserializer<T> : IXMLDeserializer<T>
        where T : IRootElement, new()
    {
        public T DeSerialize(byte[] input)
        {
            using (var stream = new MemoryStream(input))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stream);
            }
        }
    }
}
