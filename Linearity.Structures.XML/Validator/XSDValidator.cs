﻿using Linearity.Structures.Validator;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace Linearity.Structures.XML.Validator
{
    public interface IXSDValidator : IValidator<byte[], IXSDSchema, XSDValidationResult>
    {
        XSDValidationResult Validate(IXSDSchema schema, byte[] xmlToValidate, XSDValidationMode mode);
    }

    public class XSDValidator : IXSDValidator
    {
        public ILogger Logger { get; }

        public XSDValidator(ILogger logger)
        {
            Logger = logger;
        }

        public XSDValidationResult Validate(IXSDSchema schema, byte[] xmlToValidate, XSDValidationMode mode)
        {
            XSDValidationResult validationMessages = new XSDValidationResult();

            XmlReaderSettings settings = new XmlReaderSettings
            {
                ValidationType = ValidationType.Schema,
                Schemas = schema.SchemaSet,
                IgnoreProcessingInstructions = true,
            };

            try
            {
                using (var memoryStream = new MemoryStream(xmlToValidate))
                {
                    using (var reader = XmlReader.Create(memoryStream, settings))
                    {
                        var document = new XmlDocument();
                        document.Load(reader);
                        ValidationEventHandler eventHandler = new((sender, args) => XSDValidationMessageHandler(sender, args, validationMessages, mode));
                        document.Validate(eventHandler);
                    }
                }
            }
            catch (XmlSchemaValidationException xmlException)
            {
                validationMessages.Add(new XSDValidationMessage(ValidationSeverity.Error, xmlException.Message));
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
                throw;
            }

            return validationMessages;
        }

        private void LogError(string message)
        {
            if (Logger != null)
                Logger.LogError(message);
        }

        private void XSDValidationMessageHandler(object sender, ValidationEventArgs e, List<XSDValidationMessage> messages, XSDValidationMode mode)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    messages.Add(new XSDValidationMessage(ValidationSeverity.Error, e.Message));
                    break;
                case XmlSeverityType.Warning:
                    if (mode == XSDValidationMode.Strict)
                        messages.Add(new XSDValidationMessage(ValidationSeverity.Warning, e.Message));
                    break;
                default:
                    break;
            }
        }

        public XSDValidationResult Validate(IXSDSchema validationSchema, byte[] toValidate)
            => Validate(validationSchema, toValidate, XSDValidationMode.Normal);
    }
}
