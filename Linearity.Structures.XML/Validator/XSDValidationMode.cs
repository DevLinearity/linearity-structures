﻿namespace Linearity.Structures.XML
{
    public enum XSDValidationMode
    {
        Strict,
        Normal
    }
}
