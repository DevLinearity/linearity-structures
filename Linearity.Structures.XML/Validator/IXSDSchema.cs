﻿using System.Xml.Schema;
using Linearity.Structures.Validator;

namespace Linearity.Structures.XML.Validator
{
    public interface IXSDSchema : IValidationSchema
    {
        XmlSchemaSet SchemaSet { get; }
    }
}
