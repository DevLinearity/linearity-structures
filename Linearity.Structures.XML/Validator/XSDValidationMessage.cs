﻿using System.Collections;
using Linearity.Structures.Validator;

namespace Linearity.Structures.XML.Validator
{
    public class XSDValidationMessage : IValidationMessage
    {
        public XSDValidationMessage(ValidationSeverity severity, string message)
        {
            Severity = severity;
            Message = message;
        }

        public string Message { get; set; }
        public ValidationSeverity Severity { get; set; }
    }
}
