﻿using Linearity.Structures.Validator;
using System.Collections.Generic;

namespace Linearity.Structures.XML.Validator
{
    public class XSDValidationResult : List<XSDValidationMessage>, IValidationResult
    {
        public XSDValidationResult()
        {
        }

        public XSDValidationResult(IEnumerable<XSDValidationMessage> collection) : base(collection)
        {
        }

        public bool IsValid => this.Count == 0;
    }
}
