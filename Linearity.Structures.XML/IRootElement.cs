﻿using Linearity.Structures.Serializer;

namespace Linearity.Structures.XML
{
    public interface IRootElement : ISerializable
    {
    }
}
