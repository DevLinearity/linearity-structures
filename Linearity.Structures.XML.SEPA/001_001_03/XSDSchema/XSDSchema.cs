﻿using System.IO;
using System.Xml;
using System.Xml.Schema;
using Linearity.Structures.XML.SEPA.Properties;
using Linearity.Structures.XML.Validator;

namespace Linearity.Structures.XML.SEPA._001_001_03.XSDSchema
{
    public class XSDSchema : IXSDSchema
    {
        public XmlSchemaSet SchemaSet
        {
            get
            {
                XmlSchemaSet xmlSchemaSet = new XmlSchemaSet();
                var schema = Resources.pain_001_001_03;
                using (var memoryStream = new MemoryStream(schema))
                using (var reader = XmlReader.Create(memoryStream))
                {
                    xmlSchemaSet.Add("urn:iso:std:iso:20022:tech:xsd:pain.001.001.03", reader);
                }
                return xmlSchemaSet;
            }
        }
    }
}
