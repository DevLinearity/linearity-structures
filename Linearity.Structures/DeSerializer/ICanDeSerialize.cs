﻿namespace Linearity.Structures.DeSerializer
{
    public interface ICanDeSerialize<out T>
        where T : new()
    {
        T DeSerialize(byte[] input);
    }
}