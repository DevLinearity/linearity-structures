﻿namespace Linearity.Structures.Validator
{
    public enum ValidationSeverity
    {
        Info,
        Warning,
        Error
    }
}