﻿namespace Linearity.Structures.Validator
{
    public interface IValidationMessage
    {
        ValidationSeverity Severity { get; }
        string Message { get; }
    }
}