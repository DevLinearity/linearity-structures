﻿namespace Linearity.Structures.Validator
{
    public interface IValidator<in TToValidate,in TValidationSchema, out TValidationResult>
        where TValidationSchema : IValidationSchema
        where TValidationResult : IValidationResult
    {
        TValidationResult Validate(TValidationSchema validationSchema, TToValidate toValidate);
    }
}
