﻿namespace Linearity.Structures.Validator
{
    public interface IValidationResult
    {
        bool IsValid { get; }
    }
}