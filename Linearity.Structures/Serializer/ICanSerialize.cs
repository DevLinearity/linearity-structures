﻿namespace Linearity.Structures.Serializer
{
    public interface ICanSerialize<in TSerializable>
        where  TSerializable : ISerializable
    {
        byte[] Serialize(TSerializable serializable);
    }
}
