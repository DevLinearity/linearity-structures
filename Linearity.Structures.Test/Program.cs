﻿using Linearity.Structures.XML.NBB._2_1.XSDSchema;
using Linearity.Structures.XML.Serializer;
using Linearity.Structures.XML.Validator;

Console.WriteLine("Hello, World!");

////var declaration = new Linearity.Structures.XML.NBB._2_1.DeclarationRequestType
////{
////    Header = new Linearity.Structures.XML.NBB._2_1.HeaderType
////    {
////        DeclarerId = new Linearity.Structures.XML.NBB._2_1.DeclarerKBONumberDeclarerIdType
////        {
////            KBONumber = "0406290141"
////        } as Linearity.Structures.XML.NBB._2_1.DeclarerIdType,
////        FileNumber = "004",
////        FileReference = "0406290141-INC-PRD-20201211-004.xml",
////        ReportingDate = new DateTime(2020, 12, 11)
////    },
////    Customer = new Linearity.Structures.XML.NBB._2_1.CustomerType[]
////    {
////        new Linearity.Structures.XML.NBB._2_1.CustomerType
////        {
////            CustomerSequenceNumber = "1",
////            CustomerBankIdentification = "7260",
////            CustomerIdentification = new Linearity.Structures.XML.NBB._2_1.CustomerIdentificationType
////            {
////                NaturalPersonIdType = new Linearity.Structures.XML.NBB._2_1.NaturalPersonIdType
////                {
////                    RRNIdentification = "64070443463"
////                }
////            },
////            CustomerActions = new[]
////            {
////                new Linearity.Structures.XML.NBB._2_1.AddActionType
////                {
////                    Contracts = new Linearity.Structures.XML.NBB._2_1.ContractType[]
////                    {
////                        new Linearity.Structures.XML.NBB._2_1.ContractType
////                        {
////                            RelationSequenceNumber = 1,
////                            ContractTypeName = new Linearity.Structures.XML.NBB._2_1.LifeInsuranceContractTypeNameType(),
////                            CustomerStartDate = new DateTime(2020, 1, 23)
////                        }
////                    }
////                }
////            }
////        }
////    }
////};

var declaration2 = new Linearity.Structures.XML.NBB._2_1_Original.DeclarationRequestType
{
    Header = new Linearity.Structures.XML.NBB._2_1_Original.HeaderType
    {
        DeclarerId = new Linearity.Structures.XML.NBB._2_1_Original.DeclarerIdType
        {
            Item = "0406290141",
            ItemElementName = Linearity.Structures.XML.NBB._2_1_Original.ItemChoiceType.DeclarerKBONumber
        },
        FileNumber = "004",
        FileReference = "0406290141-INC-PRD-20201211-004.xml",
        ReportingDate = new DateTime(2020, 12, 11)
    },
    Customer = new Linearity.Structures.XML.NBB._2_1_Original.CustomerType[]
    {
        new Linearity.Structures.XML.NBB._2_1_Original.CustomerType
        {
            CustomerSequenceNumber = "1",
            CustomerBankIdentification = "7260",
            CustomerIdentification = new Linearity.Structures.XML.NBB._2_1_Original.CustomerIdentificationType
            {
                Item = new Linearity.Structures.XML.NBB._2_1_Original.NaturalPersonIdType
                {
                    Item = "64070443463"
                }
            },
            CustomerActions = new[]
            {
                new Linearity.Structures.XML.NBB._2_1_Original.AddActionType
                {
                    Contracts = new Linearity.Structures.XML.NBB._2_1_Original.ContractType[]
                    {
                        new Linearity.Structures.XML.NBB._2_1_Original.ContractType
                        {
                            RelationSequenceNumber = "1",
                            ContractTypeName = new Linearity.Structures.XML.NBB._2_1_Original.ContractTypeNameType
                            {
                                Item = "",
                                ItemElementName = Linearity.Structures.XML.NBB._2_1_Original.ItemChoiceType3.LifeInsurance
                            },
                            CustomerStartDate = new DateTime(2020, 1, 23)
                        }
                    }
                }
            }
        }
    }
};

////var serializer = new XMLSerializer<Linearity.Structures.XML.NBB._2_1.DeclarationRequestType>();
////var xml = serializer.Serialize(declaration);
////var validator = new XSDValidator(null);

////var validationResult = validator.Validate(new XSDSchema(), xml);

////if (validationResult.IsValid)
////{
////    File.WriteAllBytes("test.xml", xml);
////}

var serializer2 = new XMLSerializer<Linearity.Structures.XML.NBB._2_1_Original.DeclarationRequestType>();
var xml2 = serializer2.Serialize(declaration2, CustomEncodings.UTF8GovernmentEncoding);
var validator2 = new XSDValidator(null);

var validationResult2 = validator2.Validate(new XSDSchema(), xml2);

if (validationResult2.IsValid)
{
    File.WriteAllBytes("test2.xml", xml2);
}